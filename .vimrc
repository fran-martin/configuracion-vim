" Mi configuracion de usuario para Vim
"
" OPTIONS
"
" Activar el resaltado de sintaxis
syntax on

" Añadir numeracion
set number

" Añadir numeracion relativa
set relativenumber

" No copiar los numeros al seleccionar con el raton
set mouse+=a

" CONFIGURACION PARA BUSQUEDA
"
" Ignorar minusculas y mayusculas al buscar en minusculas
set ignorecase

" Resaltar coincidencias de busqueda
set hlsearch

" Resaltar el resultado de busqueda conforme vayamos introduciendo el string 
set incsearch

" CONFIGURACION PARA TABULACION
"
" Activar plugin para identacion
filetype indent plugin on

" Combertir la tabulacion a espacios
set expandtab

" Indicar cuantos espacios tiene una tabulacion
set tabstop=2

" Numero de espacios para una identacion
set shiftwidth=2

" Tamaño del historial de VIM
set history=1000
